function objFullName(obj) {
    // body...

    const fullName = Object.values(obj);
    let str = "";

    for(i = 0; i < fullName.length; i++){
        fullName[i] = fullName[i].toLowerCase();
        str = str + ' ' + fullName[i].charAt(0).toUpperCase() + fullName[i].substr(1);
    }
    return str;
}

// let test1 = {"first_name": "JoHN", "last_name": "SMith"}
// let test2 = {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}

// console.log(objFullName(test1));
// console.log(objFullName(test2));

module.exports = {
    objFullName,
}

