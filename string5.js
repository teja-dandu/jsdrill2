function arrayOfstringTostring(array) {
    // body...
    let str = "";
    if(!array) return str;
    
    for(let i = 0; i < array.length; i++){
        str = str + ' ' + array[i];
    }
    return str;
}

// console.log(arrayOfstringTostring([ 'my', 'name', 'is', 'khan' ]));
// console.log(arrayOfstringTostring());

module.exports = {
    arrayOfstringTostring,
}

