function stringIpv4ToArray(str) {
    // body...
    if(!str) return 0;

    if(str.length >= 15){
        return "Invalid IP address"
    }
    else{
        for(let i = 0; i < str.length; i++){
            let numArray = [];
            let char = str.charCodeAt(i)
            if(char !== 46 && (char < 48 || char > 57)){
                return numArray;
            }

        }
        let returnArray = str.split('.').map(Number);

        return returnArray;
    }

}
module.exports = {
    stringIpv4ToArray,
}

console.log(stringIpv4ToArray('100.234.5.7'));

