function dateStringTocurrentMonth(strDate){

    if(!strDate) return;

    if(strDate.length!==10){
        return 'give date formate as dd/mm/yyyy ';
    }
    else{
        for(let i=0;i<strDate;i++){
            if(strDate[i]<47 || strDate[i]>57)
            return 'Date cannot allowed with alphabets or any special symbols except "/"';
        }

    }

    let getMonth = strDate.substr(3,2);

    if(!((getMonth)>47) && !((getMonth<58))){
        return 'Date do not allowed special symbols except "/"';
    }
    switch(getMonth){
        case '01':
            return 'January';
            break;
        case '02':
            return 'February';
            break;
        case '03':
            return 'March';
            break;
        case '04':
            return 'April';
            break;
        case '05':
            return 'May';
            break;
        case '06':
            return 'June';
            break;
        case '07':
            return 'July';
            break;
        case '08':
            return 'August';
            break;
        case '09':
            return 'September';
            break;
        case '10':
            return 'October';
            break;
        case '11':
            return 'November';
            break;
        case '12':
            return 'December';
            break;
        default :
            return 'Invalid Month';
            break;
    }
}

module.exports = {
    dateStringTocurrentMonth,
};
console.log(dateStringTocurrentMonth('12/04/2000'));

